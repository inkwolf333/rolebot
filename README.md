# RoleBot

A discord bot that deals with role stuff, like custom role colors and pronoun roles.

---

## Adding pronouns

I know that the list on the bot (which is based on http://pronoun.is) isn't a complete list, and I don't want to leave out anyone at all.

If you want to add a pronoun to the bot, please edit the `config.json.example` file and send a PR. I will accept all serious (non "attack/helicopter") PRs.

---

**Discord banned the first rolebot as my first account was banned. You'll need to re-add rolebot to your guild if you want to keep using it.**

Invite link: https://discordapp.com/api/oauth2/authorize?client_id=543878945142210601&permissions=268435456&scope=bot

Just add it to your server, give it Manage Roles permission (should have that by default unless you uncheck it during invite), move the RoleBot role above the colored roles you want it to override, and it's good to go. 

You can set various settings for your own guild, see `rb!settings` and `rb!override`.

